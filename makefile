CC       = gcc
# compiling flags here
CFLAGS   = -std=c99 -Wall -I.

LINKER   = gcc -o
# linking flags here
LFLAGS   = -Wall -I. -lm

# change these to set the proper directories where each files should be
SRCDIR   = src
HDRDIR   = include
OBJDIR   = obj

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(HDRDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
rm       = rm -f

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

all: day1 day2 day3 day4 day5 day6 day7 day8 day9 day10 day11 day12 day13 day14 day15 day16

day1: $(OBJDIR)/day1.o
	@$(LINKER) $@ $(LFLAGS) $^

day2: $(OBJDIR)/day2.o
	@$(LINKER) $@ $(LFLAGS) $^

day3: $(OBJDIR)/day3.o
	@$(LINKER) $@ $(LFLAGS) $^

day4: $(OBJDIR)/day4.o
	@$(LINKER) $@ $(LFLAGS) $^

day5: $(OBJDIR)/day5.o $(OBJDIR)/md5.o
	@$(LINKER) $@ $(LFLAGS) $^

day6: $(OBJDIR)/day6.o
	@$(LINKER) $@ $(LFLAGS) $^

day7: $(OBJDIR)/day7.o
	@$(LINKER) $@ $(LFLAGS) $^

day8: $(OBJDIR)/day8.o
	@$(LINKER) $@ $(LFLAGS) $^

day9: $(OBJDIR)/day9.o
	@$(LINKER) $@ $(LFLAGS) $^

day10: $(OBJDIR)/day10.o
	@$(LINKER) $@ $(LFLAGS) $^

day11: $(OBJDIR)/day11.o
	@$(LINKER) $@ $(LFLAGS) $^

day12: $(OBJDIR)/day12.o
	@$(LINKER) $@ $(LFLAGS) $^

day13: $(OBJDIR)/day13.o
	@$(LINKER) $@ $(LFLAGS) $^

day14: $(OBJDIR)/day14.o $(OBJDIR)/md5.o
	@$(LINKER) $@ $(LFLAGS) $^

day15: $(OBJDIR)/day15.o
	@$(LINKER) $@ $(LFLAGS) $^

day16: $(OBJDIR)/day16.o
	@$(LINKER) $@ $(LFLAGS) $^

.PHONEY: clean
clean:
	@$(rm) $(OBJECTS)
	@echo "Cleanup complete!"
