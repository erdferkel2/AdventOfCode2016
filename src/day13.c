#include <stdio.h>
#include <stdlib.h>

#define SIZE 100
#define TARGET_X 31
#define TARGET_Y 39

int day13(int part);
void readFile(void);

typedef struct T_queue{
    int x;
    int y;
    int depth;
    struct T_queue* next;
} queue_type;

queue_type* queue = NULL;
queue_type* queue_last = NULL;

char map[SIZE][SIZE];

char hashmap[SIZE][SIZE];

int num_loc = 0;

void initHashmap(){
    for(int ii = 0; ii < SIZE; ii++){
        for(int jj = 0; jj < SIZE; jj++){
            hashmap[ii][jj] = 0;
        }
    }
}

void queuePush(int x, int y, int depth){
    if(hashmap[x][y] > 0){
        return;
    }
    else {
        hashmap[x][y] = 1;
    }
    if(depth <= 50){
        num_loc++;
    }
    if(x == TARGET_X && y == TARGET_Y){
        printf("reached target after %d steps\n",depth);
        hashmap[x][y] = 0;
        //exit(0);
    }
    if(queue == NULL){
        queue = malloc(sizeof(queue_type));
        queue->x = x;
        queue->y = y;
        queue->depth = depth;
        queue->next = NULL;
        queue_last = queue;
    }
    else {
        queue_type * qn = malloc(sizeof(queue_type));
        if(qn == NULL) printf("malloc failed\n");
        qn->x = x;
        qn->y = y;
        qn->depth = depth;
        qn->next = NULL;
        queue_last->next = qn;
        queue_last = qn;
    }
}

queue_type* queuePop(){
    if(queue == NULL){
        return NULL;
    }
    else {
        queue_type* tmp = queue;
        queue = queue->next;
        return tmp;
    }
}

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day13(part);
    }
    else{
        return day13(0);
    }
}

void generateMap(){
    int magicNumber = 1352;
    for(int ii = 0; ii < SIZE; ii++){
        for(int jj = 0; jj < SIZE; jj++){
            int cnt = 0;
            int number = ii*ii + 3*ii + 2*ii*jj + jj + jj*jj + magicNumber;

            for(int kk = 0; kk < 32; kk++){
                int bit = 1 << kk;
                if((number & bit) == bit) cnt++;
            }

            if(cnt%2 == 0)
                map[ii][jj] = 0;
            else
                map[ii][jj] = 1;
        }
    }
}

void printMap(){
    for(int ii = 0; ii < SIZE; ii++){
        for(int jj = 0; jj < SIZE; jj++){
            if(map[ii][jj] == 0)
                printf(" ");
            else
                printf("#");
        }
        printf("\n");
    }
}

void findPath(){
    queuePush(1, 1, 0);

    while(1){
        if(queue == NULL){
            return;
        }
        queue_type* q = queuePop();
        int x = q->x;
        int y = q->y;
        int depth = q->depth;

        if(x > 0) if(map[x-1][y] == 0) queuePush(x-1,y,depth+1);
        if(x < SIZE) if(map[x+1][y] == 0) queuePush(x+1,y,depth+1);
        if(y > 0) if(map[x][y-1] == 0) queuePush(x,y-1,depth+1);
        if(y < SIZE) if(map[x][y+1] == 0) queuePush(x,y+1,depth+1);

        free(q);
    }
}

int day13(int part){
    initHashmap();
    generateMap();
    findPath();

    printf("%d locations in 50 steps\n",num_loc);

    return 0;
}
