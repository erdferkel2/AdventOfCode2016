#include <stdio.h>
#include <stdlib.h>

int day9(int part);
void readFile(int part);
long int decompressPart(FILE* fp, int *cnt_ext);
long int decompressPartSingle(FILE* fp);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day9(part);
    }
    else{
        return day9(0);
    }
}

char length[10];
char times[10];

long int counter = 0;

long int decompressPart(FILE* fp,int *cnt_ext){
    char cc;
    long int counter_loc = 0;

    cc = getc(fp);
    *cnt_ext += 1;
    int cnt = 0;
    while(cc != 'x'){
        length[cnt] = cc;
        cnt++;
        cc = getc(fp);
        *cnt_ext += 1;
    }
    length[cnt] = 0;

    cnt = 0;
    cc = getc(fp);
    *cnt_ext += 1;
    while(cc != ')'){
        times[cnt] = cc;
        cnt++;
        cc = getc(fp);
        *cnt_ext += 1;
    }
    times[cnt] = 0;

    int i_length = atoi(length);
    int i_times = atoi(times);

    //read next length chars
    for(int ii = 0; ii < i_length; ii++){
        cc = getc(fp);
        *cnt_ext += 1;
        if( cc == '('){
            int cnt_advance = 0;
            counter_loc += decompressPart(fp, &cnt_advance) * i_times;
            ii += cnt_advance;
            *cnt_ext += cnt_advance;
        }
        else if(cc >= 'A' && cc <= 'Z'){
            counter_loc += i_times;
        }
    }

     return counter_loc;
}

long int decompressPartSingle(FILE* fp){
    char cc;
    long int counter_loc = 0;

    cc = getc(fp);
    int cnt = 0;
    while(cc != 'x'){
        length[cnt] = cc;
        cnt++;
        cc = getc(fp);
    }
    length[cnt] = 0;

    cnt = 0;
    cc = getc(fp);
    while(cc != ')'){
        times[cnt] = cc;
        cnt++;
        cc = getc(fp);
    }
    times[cnt] = 0;

    int i_length = atoi(length);
    int i_times = atoi(times);

    //read next length chars
    for(int ii = 0; ii < i_length; ii++){
        cc = getc(fp);
        counter_loc += i_times;
    }

     return counter_loc;
}

void readFile(int part){
    FILE* fp;
    char cc;

    fp = fopen("input/day9.txt","r");

    while(cc != EOF){
        cc = getc(fp);
        if(cc == '('){
            // start reading control sequence
            int discard = 0;
            if(part == 0){
                counter += decompressPartSingle(fp);
            }
            else{
                counter += decompressPart(fp, &discard);
            }
        }
        else if(cc >= 'A' && cc <= 'Z'){
            counter++;
        }
    }
    printf("length: %ld\n",counter);
}

int day9(int part){
    readFile(part);

    return 0;
}
