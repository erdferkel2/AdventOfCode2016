#include <stdio.h>
#include <stdlib.h>

int DISC_CNT = 6;

int day15(int part);
void input(void);
void tick(void);

int discPos[7];
int discMax[7];

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day15(part);
    }
    else{
        return day15(0);
    }
}

void findSolution(){
    int time = 0;
    int sum;
    while(1){
        sum = 0;
        for(int ii = 0; ii < DISC_CNT; ii++){
            sum += (time+ii+1+discPos[ii])%discMax[ii];
        }
        if(sum == 0){
            printf("press button @ time %d\n",time);
            break;
        }
        time++;
    }
}

void input(){
    discPos[0] = 1;
    discMax[0] = 13;
    discPos[1] = 10;
    discMax[1] = 19;
    discPos[2] = 2;
    discMax[2] = 3;
    discPos[3] = 1;
    discMax[3] = 7;
    discPos[4] = 3;
    discMax[4] = 5;
    discPos[5] = 5;
    discMax[5] = 17;
    if(DISC_CNT == 7){
        discPos[6] = 0;
        discMax[6] = 11;
    }
}

int day15(int part){
    if(part == 0){
        DISC_CNT = 6;
    }
    else {
        DISC_CNT = 7;
    }
    input();
    findSolution();

    return 0;
}
