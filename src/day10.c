#include <stdio.h>
#include <stdlib.h>

#define NUM_BOTS 300

int day10(int part);
void readFile(void);

typedef struct {
    int valueA;
    int valueB;

    int targetHigh;
    int targetLow;

    int isOutputLow;
    int isOutputHigh;

    int done;
}bot;

bot bots[NUM_BOTS];

int output[30];

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day10(part);
    }
    else{
        return day10(0);
    }
}

void initBots(){
    for(int ii = 0; ii < NUM_BOTS; ii++){
        bots[ii].valueA = 0;
        bots[ii].valueB = 0;
        bots[ii].targetHigh = 0;
        bots[ii].targetLow = 0;
        bots[ii].isOutputLow = 0;
        bots[ii].isOutputHigh = 0;
        bots[ii].done = 0;
    }
}

int getNextNumber(FILE * fp){
    char cc = getc(fp);
    int cnt = 0;
    char number[5];

    while(cc < '0' || cc > '9') cc = getc(fp);
    while(cc >= '0' && cc <= '9'){
        number[cnt] = cc;
        cc = getc(fp);
        cnt++;
    }
    number[cnt] = 0;

    return atoi(number);
}

int getOutput(FILE* fp){
    char cc;
    char last = 0;
    while(1){
        cc = getc(fp);
        if(last == 'o' && cc == 'u'){
            return 1;
        }
        if(last == 'b' && cc == 'o'){
            return 0;
        }
        last = cc;
    }
}

void readFile(){
    FILE* fp;
    char cc;

    fp = fopen("input/day10.txt","r");

    while(cc != EOF){
        cc = getc(fp);

        if(cc == 'b'){
            int botNr = getNextNumber(fp);
            int isOutputLow = getOutput(fp);
            int valueLow = getNextNumber(fp);
            int isOutputHigh = getOutput(fp);
            int valueHigh = getNextNumber(fp);

            bots[botNr].targetLow = valueLow;
            bots[botNr].targetHigh = valueHigh;

            bots[botNr].isOutputLow = isOutputLow;
            bots[botNr].isOutputHigh = isOutputHigh;
        }
        else if(cc == 'v'){
            int value = getNextNumber(fp);
            int botNr = getNextNumber(fp);

            if(bots[botNr].valueA == 0){
                bots[botNr].valueA = value;
            }
            else {
                bots[botNr].valueB = value;
            }
        }
    }
}

int evaluate(int botNr){
    printf("eval %d\n",botNr);
    if(bots[botNr].valueB == 0){
        //printf("ERROR: bot %d has only one value!\n",botNr);
        return 0;
    }
    else if(bots[botNr].done == 1){
        return 0;
    }
    else {
        if((bots[botNr].valueA == 61 && bots[botNr].valueB == 17) || (bots[botNr].valueA == 17 && bots[botNr].valueB == 61)){
            printf("bot %d comparing 61 to 17\n",botNr);
        }

        if(bots[botNr].valueA > bots[botNr].valueB){
            if(!bots[botNr].isOutputLow){
                if(bots[bots[botNr].targetLow].valueA == 0){
                    bots[bots[botNr].targetLow].valueA = bots[botNr].valueB;
                }
                else {
                    bots[bots[botNr].targetLow].valueB = bots[botNr].valueB;
                }
            }
            else{
                output[bots[botNr].targetLow] = bots[botNr].valueB;
            }
            if(!bots[botNr].isOutputHigh){
                if(bots[bots[botNr].targetHigh].valueA == 0){
                    bots[bots[botNr].targetHigh].valueA = bots[botNr].valueA;
                }
                else {
                    bots[bots[botNr].targetHigh].valueB = bots[botNr].valueA;
                }
            }
            else{
                output[bots[botNr].targetHigh] = bots[botNr].valueA;
            }
        }
        else {
            if(!bots[botNr].isOutputLow){
                if(bots[bots[botNr].targetLow].valueA == 0){
                    bots[bots[botNr].targetLow].valueA = bots[botNr].valueA;
                }
                else {
                    bots[bots[botNr].targetLow].valueB = bots[botNr].valueA;
                }
            }
            else{
                output[bots[botNr].targetLow] = bots[botNr].valueA;
            }
            if(!bots[botNr].isOutputHigh){
                if(bots[bots[botNr].targetHigh].valueA == 0){
                    bots[bots[botNr].targetHigh].valueA = bots[botNr].valueB;
                }
                else {
                    bots[bots[botNr].targetHigh].valueB = bots[botNr].valueB;
                }
            }
            else{
                output[bots[botNr].targetHigh] = bots[botNr].valueB;
            }
        }
        bots[botNr].done = 1;

        int retA = 0;
        int retB = 0;

        if(!bots[botNr].isOutputLow){
            retA = evaluate(bots[botNr].targetLow);
        }

        if(!bots[botNr].isOutputHigh){
            retB = evaluate(bots[botNr].targetHigh);
        }

        if(retA == 0){
            return retB;
        }
        else {
            return retA;
        }
    }
}

int day10(int part){
    initBots();
    readFile();
    evaluate(17); // specific to my input

    int mul = output[0]*output[1]*output[2];

    printf("Mul = %d\n",mul);

    return 0;
}
