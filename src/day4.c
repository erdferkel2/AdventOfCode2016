#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int day4(int part);
int readFile(int part);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day4(part);
    }
    else{
        return day4(0);
    }
}

int readFile(int part){
    FILE* fp;

    fp = fopen("input/day4.txt","r");

    char string[100];
    char number[10];
    int toggle = 0;

    int cnt = 0;

    int line = 0;
    char checksum[6];
    checksum[5] = 0;
    int frequency[26];

    int sum = 0;

    for(int ii = 0; ii < 26; ii++){
        frequency[ii] = 0;
    }

    while(fgets(string,sizeof(string),fp)){
        for(int ii = 0; ii < strlen(string); ii++){
            if(string[ii] == '-'){
                continue;
            }
            else if(string[ii] == '0' || string[ii] == '1' || string[ii] == '2' || string[ii] == '3' || string[ii] == '4' || string[ii] == '5' || string[ii] == '6' || string[ii] == '7' || string[ii] == '8' || string[ii] == '9'){
                number[cnt] = string[ii];
                cnt++;
                toggle = 1;
                continue;
            }
            else {
                if(toggle == 1){
                    toggle = 0;
                    number[cnt] = 0;
                    cnt = 0;
                }
            }
            if(string[ii] == '['){
                checksum[0] = string[ii+1];
                checksum[1] = string[ii+2];
                checksum[2] = string[ii+3];
                checksum[3] = string[ii+4];
                checksum[4] = string[ii+5];
                break;
            }
            else{
                frequency[string[ii]-97]++;
            }
        }

        int correct = 0;

        int max[5];
        int maxID[5];

        max[0] = 0; max[1] = 0; max[2] = 0; max[3] = 0; max[4] = 0;
        maxID[0] = 0; maxID[1] = 0; maxID[2] = 0; maxID[3] = 0; maxID[4] = 0;

        for(int jj = 0; jj < 5; jj++){

            for(int ii = 0; ii < 26; ii++){
                if(frequency[ii] > max[jj]){
                    max[jj] = frequency[ii];
                    maxID[jj] = ii;
                }
            }

            frequency[maxID[jj]] = 0;
        }

        for(int ii = 0; ii < 5; ii++){
            if(maxID[ii]+97 == checksum[ii]){
                if(ii < 4){
                    if(max[ii] == max[ii+1]){
                        if(maxID[ii] < maxID[ii+1]){
                            correct++;
                        }
                    }
                    else if(max[ii] > max[ii+1]){
                        correct++;
                    }
                }
                else{
                    correct++;
                }
            }
        }

        if(correct == 5){
            int sectorID = atoi(number);
            if(part == 0){
                sum += sectorID;
            }
            else {
                // rotate letters
                for(int ii = 0; ii < strlen(string); ii++){
                    if(string[ii] == '-'){
                        string[ii] = ' ';
                    }
                    else if(string[ii] == '0' || string[ii] == '1' || string[ii] == '2' || string[ii] == '3' || string[ii] == '4' || string[ii] == '5' || string[ii] == '6' || string[ii] == '7' || string[ii] == '8' || string[ii] == '9'){
                        string[ii] = 0;
                        break;
                    }
                    else {
                        int newChar = (int)string[ii] + sectorID%26;
                        if(newChar > 122){
                            newChar = 96 + newChar - 122;
                        }
                        string[ii] = newChar;
                    }
                }
                //printf("%s %d\n",string, sectorID);
                if(strcmp("northpole object storage ",string) == 0){
                    printf("%d\n",sectorID);
                    sum = sectorID;
                    break;
                }
            }
        }

        line++;
        for(int ii = 0; ii < 26; ii++){
            frequency[ii] = 0;
        }
    }

    if(part == 0){
        printf("Sum: %d\n",sum);
    }
    return sum;
}

int day4(int part){
    readFile(part);

    return 0;
}
