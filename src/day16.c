#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int day1(int part);
void findData(char* result, int target, char* input);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day1(part);
    }
    else{
        return day1(0);
    }
}

void expandString(char* result, int len){
    result[len] = '0';
    for(int ii = 0; ii < len; ii++){
        char tmp = result[len-ii-1];
        if(tmp == '1') tmp = '0';
        else tmp = '1';
        result[len+1+ii] = tmp;
    }
    result[len*2+1] = 0;
}

void findData(char* result, int target, char* input){
    strcpy(result, input);
    int len = strlen(result);
    while(len < target){
        expandString(result, len);
        len = len*2+1;
    }
}

void findChecksum(char* string, int length){
    string[length] = 0;
    int len = strlen(string);
    do{
        int cnt = 0;
        for(int ii = 0; ii < len; ii+=2){
            if(string[ii] == string[ii+1]) string[cnt] = '1';
            else string[cnt] = '0';
            cnt++;
        }
        string[cnt] = 0;
        len = strlen(string);
    }while(len%2 == 0);
}

int day1(int part){
    int length;

    char* result;

    if(part == 0){
        length = 272;
    }
    else {
        length = 35651584;
    }

    result = malloc(length*2*sizeof(char));

    findData(result, length, "00111101111101000");
    findChecksum(result, length);

    printf("%s\n",result);

    free(result);

    return 0;
}
