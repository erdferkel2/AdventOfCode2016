#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int day8(int part);
void readFile(void);
void initScreen(void);
int countScreen(void);
void printScreen(void);
void turnON(int x, int y);
void rotateRow(int pos, int amount);
void rotateCol(int pos, int amount);

#define SCREEN_WIDTH  50
#define SCREEN_HEIGHT 6

int screen[SCREEN_HEIGHT][SCREEN_WIDTH];

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day8(part);
    }
    else{
        return day8(0);
    }
}

void turnON(int x, int y){
    for(int ii = 0; ii < x; ii++){
        for(int jj = 0; jj < y; jj++){
            screen[jj][ii] = 1;
        }
    }
}

void rotateRow(int pos, int amount){
    for(int ii = 0; ii < amount; ii++){
        int tmp = screen[pos][SCREEN_WIDTH-1];
        for(int jj = SCREEN_WIDTH-2; jj >= 0; jj--){
            screen[pos][jj+1] = screen[pos][jj];
        }
        screen[pos][0] = tmp;
    }
}

void rotateCol(int pos, int amount){
    for(int ii = 0; ii < amount; ii++){
        int tmp = screen[SCREEN_HEIGHT-1][pos];
        for(int jj = SCREEN_HEIGHT-2; jj >= 0; jj--){
            screen[jj+1][pos] = screen[jj][pos];
        }
        screen[0][pos] = tmp;
    }
}

void readFile(){
    FILE* fp;

    char string[30];
    char * delim = " ";

    char* first;
    char* second;
    char* third;
    char* fourth;

    fp = fopen("input/day8.txt","r");

    while(fgets(string,sizeof(string),fp)){
        first = strtok(string, delim);  // rotate || rect
        second = strtok(NULL, delim);  // ?x? || row || column

        if(strcmp(first,"rect") == 0){
            if(second[1]== 'x'){ // ?x?
                turnON(second[0]-'0',second[2]-'0');
            }
            else if(second[2] == 'x'){ // ??x?
                turnON((second[0]-'0')*10+second[1]-'0',second[3]-'0');
            }
        }
        else if(strcmp(first, "rotate") == 0){
            third = strtok(NULL, delim);  // ?=?
            strtok(NULL, delim);  // by
            fourth = strtok(NULL, delim);  // number

            third += 2; // remove y= or x=

            int pos = atoi(third);
            int amount = atoi(fourth);

            if(strcmp(second,"column") == 0){
                rotateCol(pos,amount);
            }
            else if(strcmp(second, "row") == 0){
                rotateRow(pos,amount);
            }
        }
    }
}

void initScreen(void){
    for(int ii = 0; ii < SCREEN_HEIGHT; ii++){
        for(int jj = 0; jj < SCREEN_WIDTH; jj++){
            screen[ii][jj] = 0;
        }
    }
}

int countScreen(void){
    int cnt = 0;
    for(int ii = 0; ii < SCREEN_HEIGHT; ii++){
        for(int jj = 0; jj < SCREEN_WIDTH; jj++){
            if(screen[ii][jj] == 1){
                cnt++;
            }
        }
    }
    return cnt;
}

void printScreen(void){
    for(int ii = 0; ii < SCREEN_HEIGHT; ii++){
        for(int jj = 0; jj < SCREEN_WIDTH; jj++){
            if(jj%5 == 0) printf(" ");
            if(screen[ii][jj] == 0) printf(" ");
            else printf("#");
        }
        printf("\n");
    }
    printf("\n");
}

int day8(int part){
    initScreen();
    readFile();

    printScreen();

    printf("Numer: %d\n",countScreen());

    return 0;
}
