#include <stdio.h>
#include <stdlib.h>

int day2(int part);
void readFile(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day2(part);
    }
    else{
        return day2(0);
    }
}

/*
    1
  2 3 4
5 6 7 8 9
  A B C
    D
*/

void part2(){
    FILE* fp;
    char cc;

    fp = fopen("input/day2.txt","r");

    char number[10];
    int cnt = 0;

    int posX = 0;
    int posY = 0;

    while(cc != EOF){
        cc = getc(fp);
        if(cc == '\n'){
            printf("%d/%d\n",posX,posY);
            if(posX == 0){
                number[cnt] = '5';
            }
            else if(posX == 1){
                if(posY == -1){
                    number[cnt] = 'A';
                }
                else if(posY == 0){
                    number[cnt] = '6';
                }
                else if(posY == 1){
                    number[cnt] = '2';
                }
            }
            else if(posX == 2){
                if(posY == -2){
                    number[cnt] = 'D';
                }
                else if(posY == -1){
                    number[cnt] = 'B';
                }
                else if(posY == 0){
                    number[cnt] = '7';
                }
                else if(posY == 1){
                    number[cnt] = '3';
                }
                else if(posY == 2){
                    number[cnt] = '1';
                }
            }
            else if(posX == 3){
                if(posY == -1){
                    number[cnt] = 'C';
                }
                else if(posY == 0){
                    number[cnt] = '8';
                }
                else if(posY == 1){
                    number[cnt] = '4';
                }
            }
            else if(posX == 4){
                number[cnt] = '9';
            }
            cnt++;
        }
        else {
            switch (cc){
                case 'D':
                    posY--;
                    if(posX == 0 || posX == 4){
                        posY = 0;
                    }
                    else if(posX == 1 || posX == 3){
                        if(posY > 1) posY = 1;
                        if(posY < -1) posY = -1;
                    }
                    else if(posX == 2){
                        if(posY > 2) posY = 2;
                        if(posY < -2) posY = -2;
                    }
                    break;
                case 'U':
                    posY++;
                    if(posX == 0 || posX == 4){
                        posY = 0;
                    }
                    else if(posX == 1 || posX == 3){
                        if(posY > 1) posY = 1;
                        if(posY < -1) posY = -1;
                    }
                    else if(posX == 2){
                        if(posY > 2) posY = 2;
                        if(posY < -2) posY = -2;
                    }
                    break;
                case 'R':
                    posX++;
                    if(posY == 2 || posY == -2){
                        posX = 2;
                    }
                    else if(posY == 1 || posY == -1){
                        if(posX > 3) posX = 3;
                        if(posX < 1) posX = 1;
                    }
                    else if(posY == 0){
                        if(posX > 4) posX = 4;
                        if(posX < 0) posX = 0;
                    }
                    break;
                case 'L':
                    posX--;
                    if(posY == 2 || posY == -2){
                        posX = 2;
                    }
                    else if(posY == 1 || posY == -1){
                        if(posX > 3) posX = 3;
                        if(posX < 1) posX = 1;
                    }
                    else if(posY == 0){
                        if(posX > 4) posX = 4;
                        if(posX < 0) posX = 0;
                    }
                    break;
            }
        }
    }

    number[cnt] = 0;

    printf("Keycode: %s\n",number);
}

void part1(){
    FILE* fp;
    char cc;

    fp = fopen("input/day2.txt","r");

    char number[10];
    int cnt = 0;

    int posX = 0;
    int posY = 0;

    while(cc != EOF){
        cc = getc(fp);
        if(cc == '\n'){
            printf("%d/%d\n",posX,posY);
            if(posX == -1){
                if(posY == -1){
                    // 1
                    number[cnt] = '1';
                }
                else if(posY == 0){
                    // 4
                    number[cnt] = '4';
                }
                else if(posY == 1){
                    // 7
                    number[cnt] = '7';
                }
            }
            else if(posX == 0){
                if(posY == -1){
                    // 2
                    number[cnt] = '2';
                }
                else if(posY == 0){
                    // 5
                    number[cnt] = '5';
                }
                else if(posY == 1){
                    // 8,
                    number[cnt] = '8';
                }
            }
            else if(posX == 1){
                if(posY == -1){
                    // 3
                    number[cnt] = '3';
                }
                else if(posY == 0){
                    // 6
                    number[cnt] = '6';
                }
                else if(posY == 1){
                    // 9
                    number[cnt] = '9';
                }
            }
            cnt++;
        }
        else {
            switch (cc){
                case 'D':
                    posY++;
                    if(posY > 1){
                        posY = 1;
                    }
                    break;
                case 'U':
                    posY--;
                    if(posY < -1){
                        posY = -1;
                    }
                    break;
                case 'R':
                    posX++;
                    if(posX > 1){
                        posX = 1;
                    }
                    break;
                case 'L':
                    posX--;
                    if(posX < -1){
                        posX = -1;
                    }
                    break;
            }
        }
    }

    number[cnt] = 0;

    printf("Keycode: %s\n",number);
}

int day2(int part){
    if(part == 0){
        part0();
    }
    else{
        part1();
    }

    return 0;
}
