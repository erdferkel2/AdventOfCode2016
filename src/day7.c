#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int day7(int part);
void part1(void);
void part2(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day7(part);
    }
    else{
        return day7(0);
    }
}

void part1(){
    FILE* fp;
    char cc;

    fp = fopen("input/day7.txt","r");

    char prev[3];

    prev[0] = 0; prev[1] = 0; prev[2] = 0;

    int inBracket = 0;

    int invalid = 0;
    int valid = 0;

    int counter = 0;

    while(cc != EOF){
        cc = getc(fp);
        if(cc == '\n'){
            if(valid && !invalid){
                counter++;
                //printf("line %d valid\n",line);
            }
            prev[0] = 0; prev[1] = 0; prev[2] = 0;
            invalid = 0;
            valid = 0;
            inBracket = 0;
        }
        else if(cc == '['){
            inBracket = 1;
            prev[0] = prev[1];
            prev[1] = prev[2];
            prev[2] = cc;
        }
        else if(cc == ']'){
            inBracket = 0;
            prev[0] = prev[1];
            prev[1] = prev[2];
            prev[2] = cc;
        }
        else if(cc >= 'a' && cc <= 'z'){
            if(prev[0] == 0){
                prev[0] = cc;
            }
            else if(prev[1] == 0){
                prev[1] = cc;
            }
            else if(prev[2] == 0){
                prev[2] = cc;
            }
            else {
                if(prev[0] == cc && prev[1] == prev[2] && prev[0] != prev[1]){
                    // abba detected
                    if(inBracket){
                        invalid = 1;
                    }
                    else {
                        valid = 1;
                    }
                }
                prev[0] = prev[1];
                prev[1] = prev[2];
                prev[2] = cc;
            }
        }
    }
    printf("%d\n",counter);
}

int searchBAB(char* aba, char* string, int length){
    int inBracket = 0;
    char prev[2];
    prev[0] = 0; prev[1] = 0;
    for(int ii = 0; ii < length; ii++){
        if(string[ii] == '['){
            inBracket = 1;
            prev[0] = prev[1];
            prev[1] = string[ii];
        }
        else if(string[ii] == ']'){
            inBracket = 0;
            prev[0] = prev[1];
            prev[1] = string[ii];
        }
        else if(string[ii] >= 'a' && string[ii] <= 'z'){
            if(prev[0] == 0){
                prev[0] = string[ii];
            }
            else if(prev[1] == 0){
                prev[1] = string[ii];
            }
            else {
                if(prev[0] == aba[1] && prev[1] == aba[0] && string[ii] == aba[1] && inBracket){
                    return 1;
                }
                prev[0] = prev[1];
                prev[1] = string[ii];
            }
        }
    }
    return 0;
}

int searchABA(char* string, int length){
    int inBracket = 0;
    char prev[3];
    prev[0] = 0; prev[1] = 0;
    for(int ii = 0; ii < length; ii++){
        char cc = string[ii];
        if(cc == '['){
            inBracket = 1;
            prev[0] = prev[1];
            prev[1] = cc;
        }
        else if(cc == ']'){
            inBracket = 0;
            prev[0] = prev[1];
            prev[1] = cc;
        }
        else if(cc >= 'a' && cc <= 'z'){
            if(prev[0] == 0){
                prev[0] = cc;
            }
            else if(prev[1] == 0){
                prev[1] = cc;
            }
            else {
                if(prev[0] == cc && prev[0] != prev[1] && !inBracket){
                    // search for bab
                    prev[2] = cc;
                    if(searchBAB(prev, string, strlen(string))){
                        return 1;
                    }
                }
                prev[0] = prev[1];
                prev[1] = cc;
            }
        }
    }
    return 0;
}

void part2(){
    FILE* fp;
    fp = fopen("input/day7.txt","r");

    char string[150];

    int counter = 0;

    while(fgets(string,sizeof(string),fp)){
        counter += searchABA(string, strlen(string));
    }
    printf("%d\n",counter);
}

int day7(int part){
    if(part == 0){
        part1();
    }
    else{
        part2();
    }

    return 0;
}
