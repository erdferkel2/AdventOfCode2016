#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_LENGTH 23

typedef struct instr {
    char type;
    int in;
    int isRegister;
    int out;
} instr_type;

instr_type input[INPUT_LENGTH];

int day12(int part);
void readFile(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day12(part);
    }
    else{
        return day12(0);
    }
}

void readFile(){
    FILE* fp;
    char string[20];

    int line = 0;

    char* instruction;
    char* source;
    char* destination;

    char * delim = " ";

    fp = fopen("input/day12.txt","r");

    while(fgets(string,sizeof(string),fp)){
        instruction = strtok(string, delim);  // Source
        source = strtok(NULL, delim);    // =>
        destination = strtok(NULL, delim);    // Destination

        switch(string[0]){
            case 'c':
                input[line].type = 'c';
                if(source[0] >= 'a'){
                     input[line].in = *source-'a';
                     input[line].isRegister = 1;
                 }
                else {
                     input[line].in = atoi(source);
                     input[line].isRegister = 0;
                 }

                input[line].out = *destination-'a';
                break;
            case 'j':
                input[line].type = 'j';
                if(source[0] >= 'a'){
                     input[line].in = *source-'a';
                     input[line].isRegister = 1;
                 }
                else {
                     input[line].in = *source-'0';
                     input[line].isRegister = 0;
                 }
                input[line].out = atoi(destination);
                break;
            case 'd':
                input[line].type = 'd';
                input[line].out = string[4]-'a';
                break;
            case 'i':
                input[line].type = 'i';
                input[line].out = string[4]-'a';
                break;
        }
        line++;

    }
}

void printInstr(){
    for(int ii = 0; ii < INPUT_LENGTH; ii++){
        if(input[ii].type == 'c') printf("cpy %d %d\n",input[ii].in,input[ii].out);
        if(input[ii].type == 'j') printf("jnz %d %d\n",input[ii].in,input[ii].out);
        if(input[ii].type == 'd') printf("dec %d\n",input[ii].out);
        if(input[ii].type == 'i') printf("inc %d\n",input[ii].out);
    }
}

int runCode(int start, int regc){
    int pos = start;
    int reg[] = {0,0,regc,0};

    while(pos < INPUT_LENGTH){
        //printf("%d: %d %d %d %d\n",pos,reg[0],reg[1],reg[2],reg[3]);
        if(reg[1] < -100) while(1);
        switch(input[pos].type){
            case 'c':
                if(input[pos].isRegister) reg[input[pos].out] = reg[input[pos].in];
                else reg[input[pos].out] = input[pos].in;
                pos++;
                break;
            case 'j':
                if(input[pos].isRegister){
                    if(reg[input[pos].in] != 0)
                        pos+= input[pos].out;
                    else
                        pos++;
                }
                else {
                    if(input[pos].in != 0)
                        pos += input[pos].out;
                    else
                        pos++;
                }
                break;
            case 'd':
                reg[input[pos].out]--;
                pos++;
                break;
            case 'i':
                reg[input[pos].out]++;
                pos++;
                break;
        }
    }
    printf("value in register A: %d\n",reg[0]);
    return reg[0];
}

int day12(int part){
    readFile();
    //printInstr();
    if(part == 0)
        runCode(0, 0);
    else
        runCode(0, 1);

    return 0;
}
