#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#define MAX_DEPTH 100

#define BFS

static int ELEMENTS;
static int ITEM_CNT;
static int HASHMAP_SIZE;

typedef struct {
    unsigned char* floor;
} state;

typedef struct T_queue{
    int depth;
    state S;
    struct T_queue* next;
} queue_type;

int error = 0;

queue_type* queue = NULL;
queue_type* queue_last = NULL;

char* hashmap;

int day11(int part);

state input(state S);
int isValid(state S);

state copyState(state S);
unsigned int hashState(state S);

state moveUpOne(state S, int id);
state moveDownOne(state S, int id);
state moveUpTwo(state S, int idA, int idB);
state moveDownTwo(state S, int idA, int idB);

void queuePush(state S, int depth);
queue_type* queuePop();

void bruteForce_BFS();
void bruteForce_DFS();

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day11(part);
    }
    else{
        return day11(0);
    }
}

/* Floor[y]: data = level
 * y: 0 = player pos, 1..5 Gen, 6..10 chips, 1 -> 6, 2->7, etc
 */

state input(state S){
    if(ELEMENTS == 7){
        S.floor[0] = 0;
        S.floor[1] = 0;
        S.floor[2] = 0;
        S.floor[3] = 1;
        S.floor[4] = 1;
        S.floor[5] = 1;
        S.floor[6] = 0;
        S.floor[7] = 0;
        S.floor[8] = 0;
        S.floor[9] = 0;
        S.floor[10] = 1;
        S.floor[11] = 1;
        S.floor[12] = 2;
        S.floor[13] = 0;
        S.floor[14] = 0;
    }
    else if(ELEMENTS == 5){
        S.floor[0] = 0;
        S.floor[1] = 0;
        S.floor[2] = 0;
        S.floor[3] = 1;
        S.floor[4] = 1;
        S.floor[5] = 1;
        S.floor[6] = 0;
        S.floor[7] = 0;
        S.floor[8] = 1;
        S.floor[9] = 1;
        S.floor[10] = 2;
    }

    return S;
}

unsigned int hashState(state S){
    unsigned int result = 0;
    int ii;
    for(ii = 0; ii < ELEMENTS*2; ii++){
        result += S.floor[ii];  result <<= 2;
    }
    result += S.floor[ii];

    return result;
}

/* courtesy of BafTac
 */
int isValid(state S){
 unsigned char valid_pairs = 0;
 for(unsigned char ii = ELEMENTS + 1; ii < 1 + 2 * ELEMENTS; ++ii){
     unsigned char floor = S.floor[ ii ];
     if( S.floor[ ii - ELEMENTS ] == floor ){
         ++valid_pairs;
         continue;
     }

     unsigned char tmpCheck = 0;
     for(unsigned char generator = 1; generator <= ELEMENTS; ++generator){
         if( generator == ii - ELEMENTS ){
             // this chips generator, no need to check it
             continue;
         }

         if( S.floor[ generator ] != floor ){
             ++tmpCheck;
         } else {
             // already invalid, so no need to check further
             break;
         }
     }

     if( tmpCheck == ELEMENTS - 1 ){
         ++valid_pairs;
     }
 }

 return valid_pairs == ELEMENTS;
}

void queuePush(state S, int depth){
    int hash = hashState(S);
    if(hashmap[hash] > 0){
        free(S.floor);
        return;
    }
    else {
        if(hash == HASHMAP_SIZE){
            printf("reached end after %d steps\n",depth);
            exit(0);
        }
        hashmap[hash] = 1;
    }

    if(queue == NULL){
        queue = malloc(sizeof(queue_type));
        queue->S = S;
        queue->depth = depth;
        queue->next = NULL;
        queue_last = queue;
    }
    else {
        queue_type * qn = malloc(sizeof(queue_type));
        if(qn == NULL) printf("malloc failed\n");
        qn->S = S;
        qn->depth = depth;
        qn->next = NULL;
        queue_last->next = qn;
        queue_last = qn;
    }
}

queue_type* queuePop(){
    if(queue == NULL){
        return NULL;
    }
    else {
        queue_type* tmp = queue;
        queue = queue->next;
        return tmp;
    }
}

state copyState(state S){
    state N;
    N.floor = malloc(ITEM_CNT*sizeof(char));
    memcpy(N.floor,S.floor,ITEM_CNT*sizeof(char));
    return N;
}

state moveUpOne(state S, int id){
    S.floor[id]++;
    S.floor[0]++;
    char valid = isValid(S);
    S.floor[id]--;
    S.floor[0]--;

    if(!valid){
        error = 1;
        return S;
    }
    else {
        state N = copyState(S);
        N.floor[id]++;
        N.floor[0]++;
        return N;
    }
}

state moveUpTwo(state S, int idA, int idB){
    S.floor[idA]++;
    S.floor[idB]++;
    S.floor[0]++;
    char valid = isValid(S);
    S.floor[idA]--;
    S.floor[idB]--;
    S.floor[0]--;

    if(!valid){
        error = 1;
        return S;
    }
    else {
        state N = copyState(S);
        N.floor[idA]++;
        N.floor[idB]++;
        N.floor[0]++;
        return N;
    }
}

state moveDownOne(state S, int id){
    S.floor[id]--;
    S.floor[0]--;
    char valid = isValid(S);
    S.floor[id]++;
    S.floor[0]++;

    if(!valid){
        error = 1;
        return S;
    }
    else {
        state N = copyState(S);
        N.floor[id]--;
        N.floor[0]--;
        return N;
    }
}

state moveDownTwo(state S, int idA, int idB){
    S.floor[idA]--;
    S.floor[idB]--;
    S.floor[0]--;
    char valid = isValid(S);
    S.floor[idA]++;
    S.floor[idB]++;
    S.floor[0]++;

    if(!valid){
        error = 1;
        return S;
    }
    else {
        state N = copyState(S);
        N.floor[idA]--;
        N.floor[idB]--;
        N.floor[0]--;
        return N;
    }
}

void bruteForce_BFS(){
    state N;
    state S;
    int depth;

    while(1){
        if(queue == NULL){
            printf("queue empty!\n");
            return;
        }

        queue_type * q = queuePop();
        S = q->S;
        depth = q->depth;

        free(q);

        if(depth > MAX_DEPTH){
            return;
        }

        if(S.floor[0] < 3){
            for(int ii = 1; ii < ITEM_CNT; ii++){
                if(S.floor[0] == S.floor[ii]){
                    // move one item up
                    N = moveUpOne(S, ii);
                    if(error == 0){
                        queuePush(N,depth+1);
                    }
                    else {
                        error = 0;
                    }
                    for(int jj = ii+1; jj < ITEM_CNT; jj++){
                        if(S.floor[0] == S.floor[jj]){
                            if(ii==jj) continue;
                            // move two items up
                            N = moveUpTwo(S, ii, jj);
                            if(error == 0){
                                queuePush(N,depth+1);
                            }
                            else {
                                error = 0;
                            }
                        }
                    }
                }
            }
        }
        if(S.floor[0] > 0){
            for(int ii = 1; ii < ITEM_CNT; ii++){
                if(S.floor[0] == S.floor[ii]){
                    // move one item down
                    N = moveDownOne(S, ii);
                    if(error == 0){
                        queuePush(N,depth+1);
                    }
                    else {
                        error = 0;
                    }
                    for(int jj = ii+1; jj < ITEM_CNT; jj++){
                        if(S.floor[0] == S.floor[jj]){
                            if(ii==jj) continue;
                            // move two items down
                            N = moveDownTwo(S, ii, jj);
                            if(error == 0){
                                queuePush(N,depth+1);
                            }
                            else {
                                error = 0;
                            }
                        }
                    }
                }
            }
        }

        free(S.floor);
    }
}

void bruteForce_DFS(state S, int depth){
    state N;

    int hash = hashState(S);
    if(hashmap[hash] < depth){
        free(S.floor);
        return;
    }
    else {
        if(hash == HASHMAP_SIZE){
            printf("reached end after %d steps\n",depth);
        }
        hashmap[hash] = depth;
    }

    if(depth > MAX_DEPTH){
        return;
    }

    if(S.floor[0] < 3){
        for(int ii = 1; ii < ITEM_CNT; ii++){
            if(S.floor[0] == S.floor[ii]){
                // move one item up
                N = moveUpOne(S, ii);
                if(error == 0){
                    bruteForce_DFS(N,depth+1);
                }
                else error = 0;
                for(int jj = ii+1; jj < ITEM_CNT; jj++){
                    if(S.floor[0] == S.floor[jj]){
                        if(ii==jj) continue;
                        // move two items up
                        N = moveUpTwo(S, ii, jj);
                        if(error == 0){
                            bruteForce_DFS(N,depth+1);
                        }
                        else error = 0;
                    }
                }
            }
        }
    }
    if(S.floor[0] > 0){
        for(int ii = 1; ii < ITEM_CNT; ii++){
            if(S.floor[0] == S.floor[ii]){
                // move one item down
                N = moveDownOne(S, ii);
                if(error == 0){
                    bruteForce_DFS(N,depth+1);
                }
                else error = 0;
                for(int jj = ii+1; jj < ITEM_CNT; jj++){
                    if(S.floor[0] == S.floor[jj]){
                        if(ii==jj) continue;
                        // move two items down
                        N = moveDownTwo(S, ii, jj);
                        if(error == 0){
                            bruteForce_DFS(N,depth+1);
                        }
                        else error = 0;
                    }
                }
            }
        }
    }
    free(S.floor);
}

int day11(int part){
    if(part == 0){
        ELEMENTS = 5;
    }
    else {
        ELEMENTS = 7;

    }
    ITEM_CNT = ELEMENTS*2+1;
    HASHMAP_SIZE = ( 1 << ((ITEM_CNT)*2))-1;

    hashmap = calloc(HASHMAP_SIZE, sizeof(char));

    state start;
    start.floor = malloc(ITEM_CNT*sizeof(char));
    start = input(start);

#ifdef BFS
    queuePush(start, 0);
    bruteForce_BFS();
#else
    bruteForce_DFS(start,0);
#endif

    free(hashmap);

    return 0;
}
