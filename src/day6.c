#include <stdio.h>
#include <stdlib.h>

int day6(int part);
void readFile(int part);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day6(part);
    }
    else{
        return day6(0);
    }
}

void readFile(int part){
    FILE* fp;
    char cc;

    int accumulator[8][26];

    for(int ii = 0; ii < 8; ii++){
        for(int jj = 0; jj < 26; jj++){
            accumulator[ii][jj] = 0;
        }
    }

    fp = fopen("input/day6.txt","r");

    int counter = 0;

    while(cc != EOF){
        cc = getc(fp);
        if(cc == '\n'){
            counter = 0;
            continue;
        }

        if(cc-'a' < 0){
            continue;
        }

        accumulator[counter][cc - 'a']++;

        counter++;
    }

    int max = 0;
    int maxID = 0;

    char result[9];

    result[8] = 0;

    if(part == 0){
        for(int ii = 0; ii < 8; ii++){
            max = 0;
            maxID = 0;
            for(int jj = 0; jj < 26; jj++){
                if(accumulator[ii][jj] > max){
                    max = accumulator[ii][jj];
                    maxID = jj;
                }
            }
            result[ii] = (char) maxID + 'a';
        }
    }
    else {
        for(int ii = 0; ii < 8; ii++){
            max = 99999;
            maxID = 0;
            for(int jj = 0; jj < 26; jj++){
                if(accumulator[ii][jj] < max){
                    max = accumulator[ii][jj];
                    maxID = jj;
                }
            }
            result[ii] = (char) maxID + 'a';
        }
    }
    printf("Message: %s\n",result);
}

int day6(int part){
    readFile(part);

    return 0;
}
