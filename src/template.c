#include <stdio.h>
#include <stdlib.h>

int day1(int part);
void readFile(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day1(part);
    }
    else{
        return day1(0);
    }
}

void readFile(){
    FILE* fp;
    char cc;

    fp = fopen("input/day1.txt","r");

    while(cc != EOF){
        cc = getc(fp);

    }
}

int day1(int part){
    readFile();

    return 0;
}
