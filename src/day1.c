#include <stdio.h>
#include <stdlib.h>

int day1(int part);
int readFile(int part);
int day1_part1(void);
int day1_part2(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day1(part);
    }
    else{
        return day1(0);
    }
}

int day1_part2(void){
    FILE* fp;
    char cc;

    fp = fopen("input/day1.txt","r");

    int posX[1000];
    int posY[1000];
    int dir  = 0; // 0=North, 1=East, 2=South, 3=West

    int pos = 0;

    posX[0] = 0;
    posY[0] = 0;

    while(cc != EOF){
        cc = getc(fp);
        if(cc == 'R'){
            dir++;
            if(dir > 3) dir = 0;
        }
        else if(cc == 'L'){
            dir--;
            if(dir < 0) dir = 3;
        }
        else if(cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7' || cc == '8' || cc == '9'){
            int cnt = 0;
            char number[5];
            while(cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7' || cc == '8' || cc == '9'){
                number[cnt] = cc;
                cnt++;
                cc = getc(fp);
            }
            number[cnt] = 0;
            int dist = atoi(number);

            for(int ii = 0; ii < dist; ii++){
                pos++;

                switch(dir){
                    case 0:
                        posX[pos] = posX[pos-1] + 1;
                        posY[pos] = posY[pos-1];
                        break;
                    case 1:
                        posX[pos] = posX[pos-1];
                        posY[pos] = posY[pos-1] + 1;
                        break;
                    case 2:
                        posX[pos] = posX[pos-1] - 1;
                        posY[pos] = posY[pos-1];
                        break;
                    case 3:
                        posX[pos] = posX[pos-1];
                        posY[pos] = posY[pos-1] - 1;
                        break;
                }
                for(int jj = 0; jj < pos; jj++){
                    if(posX[jj] == posX[pos] && posY[jj] == posY[pos]){
                        goto done;
                    }
                }
            }
        }
    }

    done:

    if(posX[pos] < 0) posX[pos] = -posX[pos];
    if(posY[pos] < 0) posY[pos] = -posY[pos];

    printf("distance: %d\n",posX[pos]+posY[pos]);
    return posX[pos]+posY[pos];
}

int day1_part1(void){
    FILE* fp;
    char cc;

    fp = fopen("input/day1.txt","r");

    int posX = 0;
    int posY = 0;
    int dir  = 0; // 0=North, 1=East, 2=South, 3=West

    while(cc != EOF){
        cc = getc(fp);
        if(cc == 'R'){
            dir++;
            if(dir > 3) dir = 0;
        }
        else if(cc == 'L'){
            dir--;
            if(dir < 0) dir = 3;
        }
        else if(cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7' || cc == '8' || cc == '9'){
            int cnt = 0;
            char number[5];
            while(cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7' || cc == '8' || cc == '9'){
                number[cnt] = cc;
                cnt++;
                cc = getc(fp);
            }
            number[cnt] = 0;
            int dist = atoi(number);

            switch(dir){
                case 0:
                    posX += dist;
                    break;
                case 1:
                    posY += dist;
                    break;
                case 2:
                    posX -= dist;
                    break;
                case 3:
                    posY -= dist;
                    break;
            }
        }
    }

    if(posX < 0) posX = -posX;
    if(posY < 0) posY = -posY;

    printf("distance: %d\n",posX+posY);
    return posX+posY;
}

int day1(int part){
    if(part == 0){
        return day1_part1();
    }
    else {
        return day1_part2();
    }
}
