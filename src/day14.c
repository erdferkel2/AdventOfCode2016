#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

typedef struct T_list{
    int age;
    char cc;
    struct T_list* next;
} list_type;

int day14(int part);
void findKey(int part);
char findQuintuples(char* string, int length);
char findTriple(char* string, int length);
int checkList(list_type* elem, char cc);
void insertList(char cc);
void tickList();

list_type* list = NULL;
list_type* list_last = NULL;

int counter = 0;
int keyCnt = 0;

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day14(part);
    }
    else{
        return day14(0);
    }
}

char findQuintuples(char* string, int length){
    char first = string[0];
    char second = string[1];
    char third = string[2];
    char fourth = string[3];
    for(int ii = 4; ii < length; ii++){
        if(first == second && second == third && third == fourth && fourth == string[ii]){
            // found quintuple, check match in list
            return checkList(list, first);
        }
        first = second;
        second = third;
        third = fourth;
        fourth = string[ii];
    }
    return 0;
}

char findTriple(char* string, int length){
    char first = string[0];
    char second = string[1];
    for(int ii = 2; ii < length; ii++){
        if(first == second && second == string[ii]){
            return first;
        }
        first = second;
        second = string[ii];
    }
    return 0;
}

int checkList(list_type* elem, char cc){
    if(elem == NULL){
        return 0;
    }
    if(elem->cc == cc){
        keyCnt++;
        printf("key %d @ %d\n",keyCnt,elem->age);
        elem->age = 0;
        return 1+checkList(elem->next, cc);
    }
    return checkList(elem->next, cc);
}

void insertList(char cc){
    //printf("add %c to list\n",cc);
    if(list == NULL){
        list = malloc(sizeof(list_type));
        list->cc = cc;
        list->age = counter;
        list->next = NULL;
        list_last = list;
    }
    else {
        list_type * qn = malloc(sizeof(list_type));
        qn->cc = cc;
        qn->age = counter;
        qn->next = NULL;
        list_last->next = qn;
        list_last = qn;
    }
}

void tickList(){
    if(list == NULL){
        return;
    }
    else {
        if((counter - list->age) > 1000){
            list_type* tmp = list;
            list = list->next;
            //printf("%c expired\n",tmp->cc);
            free(tmp);
        }
    }
}
void findKey(int part){
    //char* salt = "yjdafjpo";
    //char* salt = "cuanljph"; // baftac
    //char* salt = "ihaygndm"; // franz
    char* salt = "abc"; // testcase

    unsigned char digest[16];
    char string[33];
    char md5string[33];
    MD5_CTX context;

    while(counter < 100){
        strcpy(string, "");
        sprintf(string, "%s%d",salt, counter);

        int iter = 1;

        if(part == 0) iter = 1;
        else iter = 2017;

        for(int ii = 0; ii < iter; ii++){
            MD5_Init(&context);
            MD5_Update(&context, string, strlen(string));
            MD5_Final(digest, &context);

            for(int ii = 0; ii < 16; ++ii)
                sprintf(&md5string[ii*2], "%02x", (unsigned int)digest[ii]);

            strcpy(string, md5string);
        }

        printf("%s\n",md5string);

        char dd = findQuintuples(md5string, 33);
        char cc = findTriple(md5string, 33);

        if(cc > 0) printf("triple @ %d (%s)\n",counter, md5string);

        if(cc > 0) insertList(cc);

        // if(dd > 0) printf("qint @ %d (%s)\n",counter,md5string);

        if(keyCnt >= 64){
            //printf("found key #64 @ %d\n",counter);
            break;
        }

        tickList();
        counter++;
    }
}

int day14(int part){
    findKey(part);

    return 0;
}
