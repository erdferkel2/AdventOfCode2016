#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int day3(int part);
void readFile(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day3(part);
    }
    else{
        return day3(0);
    }
}

int part1(void);
int part2(void);

int part1(){
    FILE* fp;
    char cc;

    fp = fopen("input/day3.txt","r");

    char string[20];
    char * delim = " ";

    char * number_0;
    char * number_1;
    char * number_2;
    int number[3];

    int counter = 0;

    while(fgets(string,sizeof(string),fp)){
        number_0 = strtok(string, delim);
        number_1 = strtok(NULL, delim);
        number_2 = strtok(NULL, delim);

        number[0] = atoi(number_0);
        number[1] = atoi(number_1);
        number[2] = atoi(number_2);

        if(number[0] + number[1] > number[2] && number[0] + number[2] > number[1] && number[1] + number[2] > number[0]){
            counter++;
        }
    }

    printf("number: %d\n",counter);

    return counter;
}

int part2(){
    FILE* fp;
    char cc;

    fp = fopen("input/day3.txt","r");

    char string[20];
    char * delim = " ";

    char * number_0;
    char * number_1;
    char * number_2;


    int number[3][3];

    int counter = 0;

    int line = 0;

    while(fgets(string,sizeof(string),fp)){
        number_0 = strtok(string, delim);
        number_1 = strtok(NULL, delim);
        number_2 = strtok(NULL, delim);

        number[line][0] = atoi(number_0);
        number[line][1] = atoi(number_1);
        number[line][2] = atoi(number_2);

        line++;

        if(line == 3){
            if(number[0][0] + number[1][0] > number[2][0] && number[0][0] + number[2][0] > number[1][0] && number[1][0] + number[2][0] > number[0][0]){
                counter++;
            }
            if(number[0][1] + number[1][1] > number[2][1] && number[0][1] + number[2][1] > number[1][1] && number[1][1] + number[2][1] > number[0][1]){
                counter++;
            }
            if(number[0][2] + number[1][2] > number[2][2] && number[0][2] + number[2][2] > number[1][2] && number[1][2] + number[2][2] > number[0][2]){
                counter++;
            }
            line = 0;
            printf("%d %d %d\n",number[0][0],number[1][0],number[2][0]);
            printf("%d %d %d\n",number[0][1],number[1][1],number[2][1]);
            printf("%d %d %d\n",number[0][2],number[1][2],number[2][2]);
        }
    }

    printf("number: %d\n",counter);

    return counter;
}

int day3(int part){
    if(part == 0){
        return part1();
    }
    else {
        return part2();
    }
}
