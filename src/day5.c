#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "md5.h"

#define CINEMATIC

int day5(int part);
void readFile(int part);
void cinematic_print(char* pwd);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day5(part);
    }
    else{
        return day5(0);
    }
}

void cinematic_print(char* pwd){
    printf("cracking password: ");
    for(int ii = 0; ii < 8; ii++){
        if(pwd[ii] == 0){
            // random char
            char randchar = 'a' + (rand() % 26);
            printf("%c", randchar);
        }
        else {
            printf("%c",pwd[ii]);
        }
    }

    printf("\r");
}

void readFile(int part){
    FILE* fp;

    fp = fopen("input/day5.txt","r");

    unsigned char digest[16];
    char string[33];
    char md5string[33];
    MD5_CTX context;

    char doorID[15];

    fgets(doorID,sizeof(doorID),fp);

    #ifdef CINEMATIC
    srand(time(NULL));
    #endif

    // remove \n
    char *pos;
    if ((pos=strchr(doorID, '\n')) != NULL)
    *pos = '\0';

    char pw[9];

    for(int ii = 0; ii < 9; ii++){
        pw[ii] = 0;
    }

    int pwd_len = 0;
    int zeroes = 0;
    int counter = 0;

    while(pwd_len < 8){
        zeroes = 0;

        counter++;

        strcpy(string, "");
        sprintf(string, "%s%d",doorID, counter);

    #ifdef CINEMATIC
        if(counter%500 == 0){
            cinematic_print(pw);
        }
    #endif

        MD5_Init(&context);
        MD5_Update(&context, string, strlen(string));
        MD5_Final(digest, &context);

        for(int ii = 0; ii < 16; ++ii)
            sprintf(&md5string[ii*2], "%02x", (unsigned int)digest[ii]);

        for(int ii = 0; ii < 5; ii++){
            if(md5string[ii] != '0'){
                break;
            }
            else{
                zeroes++;
            }
        }

        if(part == 0){
            if(zeroes == 5){
                pw[pwd_len] = md5string[5];
                pwd_len++;
                //printf("found char: %c @ %d with string %s\n",pw[pwd_len-1], counter,string);
            }
        }
        else{
            if(zeroes == 5){
                if(md5string[5] <= '7' && md5string[5] >= '0'){
                    if(pw[md5string[5]-'0'] == 0){

                        pw[md5string[5]-'0'] = md5string[6];
                        pwd_len++;
                        //printf("found char: %c @ %d with string %s and hasn %s\n",pw[md5string[5]-'0'], counter,string, md5string);
                    }
                }
            }
        }
    }

    #ifdef CINEMATIC
    printf("cracking password: %s\nDONE!\n",pw);
    #else
    printf("%s\n",pw);
    #endif

}

int day5(int part){
    readFile(part);

    return 0;
}
